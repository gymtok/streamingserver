const express = require("express");
const app = express();
const fs = require("fs");
const multer = require("multer");
const bodyParser = require("body-parser");

const cors = require("cors");

const upload = multer({
  dest: "videos/",
  storage: multer.memoryStorage(),
});
app.use(bodyParser.json());
app.use(cors());

app.get("/:id", function (req, res) {
  const videoName = req.params.id;
  console.log("requesting " + __dirname + `/videos/${videoName}`);
  res.sendFile(__dirname + `/videos/${videoName}`);
});

app.post("/upload-video", upload.single("uplodedFile"), (req, res) => {
  const fileName = req.file.originalname;
  const path = `${__dirname}\\videos\\${fileName}.mp4`;

  fs.writeFileSync(path, req.file.buffer);
  console.log("sending .... ", fileName);
  res.send(`${req.file.originalname}.mp4`);
});
app.listen(8000, function () {
  console.log("Listening on port 8000!");
});
